Full featured email server in a box
========

---

## Philosophy
I have used many email servers that are out there using docker like tival or mail-in-a-box which uses a fresh OS install.  I wanted something that I could mold to my shape an is flexible.  Hence docker-2-email.

## Out of the box 
spamassasin works to limit spam and sieve automatically files emails marked into spam.  I love the GMAIL philosopy of accept everything and let the user decide what what is junk.  Compared to the more strick recieving policies of
Yahoo or AOL which limit acceptance to strick criteria.

## Here is what I do not impliment:  
Postgrey becuase emails that should be recieved immediatly are deferred which make them unsuable.  i.e. password reset emails or new accout signup emails which are time sensetive and deferring them renders
them usless because they are usually time sensetive.  I also do not impliment amavis.  This is for not other reason than system resources.  Amavis simply is a big resource hog.

## Here is what I do impiment:
Opendkim, rDNS verification, an 1 RBL to block senders using dynamic ips.

--- 
## What is this made of?
1. **Postfix** smtp server
2. **dovecot-imap** lmtp, sasl auth 
3. **OpenDKIM** DKIM signing
4. **Vimbadmin** user management  
5. **Solr** FTS for dovecot, GMAIL like superfast searches to find messages 
6. **Rainloop** webmail client that is very flexible.

---
## Usage

`git clone https://nrmx@bitbucket.org/nrmx/docker-2-email.git`

- **configure docker-compose.yml**
