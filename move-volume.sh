#!/bin/bash
echo "VOLUME:"
read VOLUME
echo "TARGET_HOST:"
read TARGET_HOST

docker run --rm -v $VOLUME:/from alpine ash -c "cd /from ; tar -cf - . " | ssh $TARGET_HOST 'docker run --rm -i -v $VOLUME:/to alpine ash -c "cd /to ; tar -xpvf - " '
