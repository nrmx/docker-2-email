#!/bin/bash
                                                                                                  
echo "--------------------- STARTUP RUNNING----------------------------------------------------------------"
#RUN EVERYTIME THE SERVER STARTS
#CREATE KEYS ON REBOOT
function sedeasy {
  sed -i "s/$(echo $1 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')/$(echo $2 | sed -e 's/[\/&]/\\&/g')/g" $3
}

output=$(mysql vimbadmin -N -uroot -p$ROOTPW -hmysql -se "SELECT domain FROM domain")
output=($output)
for domain in "${output[@]}"
do
        domain="$(echo -e "${domain}" | tr -d '[:space:]')"
        if [[ -z $domain ]]
        then
          continue
        fi

        dkim="/etc/opendkim/keys/$domain"

        if [[ ! -d $dkim ]]
        then
          echo "Creating OpenDKIM folder $dkim"
          mkdir -p $dkim
          cd $dkim && opendkim-genkey -s mail -d $domain
          chown -R opendkim:opendkim /etc/opendkim/keys/

          if grep -q inai "/etc/opendkim/TrustedHosts"; then echo echo -e "*.$domain" >> /etc/opendkim/TrustedHosts
          else
                echo -e "*.$domain" >> /etc/opendkim/TrustedHosts
          fi
          echo "*@$domain mail._domainkey.$domain" >> /etc/opendkim/SigningTable
          echo "mail._domainkey.$domain $domain:mail:$dkim/mail.private" >> /etc/opendkim/KeyTable
          cat "$dkim/mail.txt"
        fi
done

chown -R opendkim:opendkim /etc/opendkim

#Fix fail2ban if not shutdown correctly
if [ -f /var/run/fail2ban/fail2ban.sock ]; then
    rm /var/run/fail2ban/fail2ban.sock
    echo "fail2ban.sock removed"
fi


#SmartHost Config
#!/bin/bash
#SmartHost Config
if [ ! -z ${SMART_HOST_PORT} ] ; then
        echo "ADDING..."
        sed -i '/#SMARTHOST/d' /etc/postfix/main.cf
        sed -i '/smtp_sasl_auth_enable/d' /etc/postfix/main.cf
        sed -i '/smtp_sasl_security_options/d' /etc/postfix/main.cf
        sed -i '/smtp_use_tls/d' /etc/postfix/main.cf
        sed -i '/relayhost/d' /etc/postfix/main.cf
        sed -i '/smtp_sasl_password_maps/d' /etc/postfix/main.cf
        sed -i '/smtp_tls_CAfile/d' /etc/postfix/main.cf

cat <<EOT >> /etc/postfix/main.cf

#SMARTHOST
smtp_sasl_auth_enable = yes
smtp_sasl_security_options = noanonymous
smtp_use_tls = yes
relayhost = [$SMART_HOST]:$SMART_HOST_PORT
smtp_sasl_password_maps = static:$SMART_H_USER:$SMART_H_PASSWORD
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
EOT
else
        echo "DELETING..."
        sed -i '/#SMARTHOST/d' /etc/postfix/main.cf
        sed -i '/smtp_sasl_auth_enable/d' /etc/postfix/main.cf
        sed -i '/smtp_sasl_security_options/d' /etc/postfix/main.cf
        sed -i '/smtp_use_tls/d' /etc/postfix/main.cf
        sed -i '/relayhost/d' /etc/postfix/main.cf
        sed -i '/smtp_sasl_password_maps/d' /etc/postfix/main.cf
        sed -i '/smtp_tls_CAfile/d' /etc/postfix/main.cf

fi

touch /var/log/auth.log
