#!/bin/bash
printenv
echo "################FIRST RUN START#################################"
#SIEVE
chown -R vmail:vmail /var/lib/dovecot
chown -R vmail:vmail /var/sieve-scripts
sievec /var/lib/dovecot/sieve/default.sieve

#DKIM
domain=$MAILNAME
dkim="/etc/opendkim/keys/$domain"
if [ ! -d "$dkim" ]; then
	echo "###Creating OpenDKIM folder $dkim###"
	mkdir -p $dkim
	cd $dkim && opendkim-genkey -s mail -d $domain
	chown -R opendkim:opendkim /etc/opendkim/keys/
	echo -e "$domain" >> /etc/opendkim/TrustedHosts
	echo "*@$domain mail._domainkey.$domain" >> /etc/opendkim/SigningTable
	echo "mail._domainkey.$domain $domain:mail:$dkim/mail.private" >> /etc/opendkim/KeyTable
	cat "$dkim/mail.txt"
	sed -i "s/MAILNAME/$MAILNAME/g" /etc/postfix/main.cf
	rm -rf /etc/my_init.d/first_run.sh
	fi
echo "###folder $dkim exists###"
sed -i "s/MYSQL_DB_NAME/$MYSQL_DB_NAME/g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s/MYSQL_DB_USERNAME/$MYSQL_DB_USERNAME/g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s/MYSQL_DB_PASSWORD/$MYSQL_DB_PASSWORD/g" /etc/dovecot/dovecot-sql.conf.ext
sv restart dovecot

sed -i "s/MYSQL_DB_PASSWORD/$MYSQL_DB_PASSWORD/g" /etc/postfix/mysql/*.cf
sed -i "s/MYSQL_DB_USERNAME/$MYSQL_DB_USERNAME/g" /etc/postfix/mysql/*.cf
sed -i "s/MYSQL_DB_NAME/$MYSQL_DB_NAME/g" /etc/postfix/mysql/*.cf
sv stop postfix
sv start postfix

if [ "$LE" -eq "1" ]; then
    sed -i "s/\/etc\/ssl\/private\/server.key/\/etc\/letsencrypt\/live\/$MAILNAME\/privkey.pem/g" /etc/postfix/main.cf
    sed -i "s/\/etc\/ssl\/certs\/server.crt/\/etc\/letsencrypt\/live\/$MAILNAME\/fullchain.pem/g" /etc/postfix/main.cf
    
    sed -i "s/\/etc\/ssl\/private\/server.key/\/etc\/letsencrypt\/live\/$MAILNAME\/privkey.pem/g" /etc/dovecot/dovecot.conf
    sed -i "s/\/etc\/ssl\/certs\/server.crt/\/etc\/letsencrypt\/live\/$MAILNAME\/fullchain.pem/g" /etc/dovecot/dovecot.conf
   exit;
fi



if [ $? -eq 0 ]; then
    echo "################FIRST RUN COMPLETE#################################"
else
	echo "################FIRST RUN X#################################"    
fi
