#!/bin/bash
subj="/C=US/ST=Denial/L=Springfield/O=Dis/CN=localhost"
if [ -f /etc/ssl/certs/dovecot.pem ]
then
  openssl req -new -x509 -days 3650 -nodes -out /etc/ssl/certs/dovecot.pem -keyout /etc/ssl/private/dovecot.pem -subj $subj
fi

if [[ ! -a '/etc/ssl/certs/postfix.pem' ]]
then
  openssl req -new -x509 -days 3650 -nodes -out /etc/ssl/certs/server.crt -keyout /etc/ssl/private/server.key -subj $subj
fi

groupadd -g 5000 vmail > /dev/null
useradd -u 5000 -g 5000 -s /bin/bash vmail > /dev/null
usermod -G opendkim postfix

test -d /etc/opendkim/keys || mkdir -p /etc/opendkim/keys
test -f /etc/opendkim/TrustedHosts || touch /etc/opendkim/TrustedHosts
test -f /etc/opendkim/KeyTable || touch /etc/opendkim/KeyTable
test -f /etc/opendkim/SigningTable || touch /etc/opendkim/SigningTable

echo -e 'SOCKET="inet:12301@localhost"\n' > /etc/default/opendkim

        if [[ ! -d "/home/vmail" ]]; then
                mkdir /home/vmail
                chown 5000:5000 /home/vmail
                chmod 700 /home/vmail
            else
                chown 5000:5000 /home/vmail
                chmod 700 /home/vmail
        fi

