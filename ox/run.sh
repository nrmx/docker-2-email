#!/bin/bash
if [ -f /first_run.sh ]; then
    bash /first_run.sh
    rm -rf /first_run.sh
else
    sudo -u open-xchange /opt/open-xchange/sbin/open-xchange &
    service apache2 start
    service mysql start
    tail -f /var/log/apache2/other_vhosts_access.log
fi
