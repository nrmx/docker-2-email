#!/bin/sh

sleep 80s
APPLICATION_INI_PATH="$APPLICATION_PATH/application/configs/application.ini"

sed -i 's/__DB_HOST__/'$MYSQL_PORT_3306_TCP_ADDR'/'    $APPLICATION_INI_PATH
# NB: 2015-02-02 - due to #124, the database currently needs to be called vimbadmin.
sed -i 's/__MYSQL_DB_NAME__/'$MYSQL_DB_NAME'/'         $APPLICATION_INI_PATH
sed -i 's/__MYSQL_DB_USERNAME__/'$MYSQL_DB_USERNAME'/' $APPLICATION_INI_PATH
sed -i 's/__MYSQL_DB_PASSWORD__/'$MYSQL_DB_PASSWORD'/' $APPLICATION_INI_PATH

cd $APPLICATION_PATH/
cp $APPLICATION_PATH/public/.htaccess.dist $APPLICATION_PATH/public/.htaccess


# Create the database and the user if doesn't exist
if ! mysql -s -h $MYSQL_PORT_3306_TCP_ADDR -p$MYSQL_ROOT_PASSWORD -u root -e "use $MYSQL_DB_NAME"; then
  echo "Creating the $MYSQL_DB_NAME database on server $MYSQL_PORT_3306_TCP_ADDR ..."
  Q1="CREATE DATABASE $MYSQL_DB_NAME;"
  Q2="GRANT ALL ON *.* TO '$MYSQL_DB_USERNAME' IDENTIFIED BY '$MYSQL_DB_PASSWORD';"
  Q3="FLUSH PRIVILEGES;"
  SQL="${Q1}${Q2}${Q3}"
  mysql -s -h $MYSQL_PORT_3306_TCP_ADDR -p$MYSQL_ROOT_PASSWORD -u root -e "$SQL"
  # Create the database tables
  ./bin/doctrine2-cli.php orm:schema-tool:create

  HASH_PASS=`php -r "echo password_hash('$VIMBADMIN_SUPERUSER_PASSWORD', PASSWORD_DEFAULT);"`
  mysql -u $MYSQL_DB_USERNAME -p$MYSQL_DB_PASSWORD -h $MYSQL_PORT_3306_TCP_ADDR $MYSQL_DB_NAME -e \
    "INSERT INTO admin (username, password, super, active, created, modified) VALUES ('$VIMBADMIN_SUPERUSER', '$HASH_PASS', 1, 1, NOW(), NOW())" && \ 
  mysql -u $MYSQL_DB_USERNAME -p$MYSQL_DB_PASSWORD -h $MYSQL_PORT_3306_TCP_ADDR $MYSQL_DB_NAME -e \
    "INSERT INTO domain (domain, created) VALUES ('$DOMAIN', NOW())" && \
  echo "Vimbadmin setup completed successfully"
fi




#<?php
#Create user
#$pasword = "ebay";
#$salt = substr(sha1(rand()), 0, 16);
#$hashedPassword = crypt($password, "$6$$salt");
#//$hashedPassword = substr($hashedPassword, 0, strpos($variable, "/"));
#echo $hashedPassword;
#
#$servername = "mysql";
#$username = "vimbadmin";
#$password = "r1X";
#$dbname = "vimbadmin";
#$email = "youremail@nyco.cc";
#$domain_name = substr(strrchr($email, "@"), 1);
#$user = strstr($email, '@', true);
#$homedir =  "/home/vmail/".$domain_name."/".$user;
#$maildir =  "/home/vmail/".$domain_name."/".$user."mail:LAYOUT=fs";
#$date = date("Y-m-d H:i:s");
#// Create connection
#$conn = new mysqli($servername, $username, $password, $dbname);
#$sql = "INSERT INTO  mailbox (username, password, local_part, active, homedir, maildir, uid, gid, Domain_id, created)
#VALUES ('$email', '$hashedPassword', '$user', 1, '$homedir', '$maildir', 5000, 5000, 2, '$date')";
#if ($conn->query($sql) === TRUE) {
#    echo "New record created successfully";
#} else {
#    echo "Error: " . $sql . "<br>" . $conn->error;
#}
#
#$conn->close();
