FROM phusion/baseimage:latest
MAINTAINER Paulo Cesar <email@pocesar.e4ward.com>

# Use baseimage-docker's init system.
ENV DEBIAN_FRONTEND noninteractive

RUN add-apt-repository ppa:pdoes/dovecot -y
RUN apt-get update -y -q && apt-get dist-upgrade -y -q

RUN  echo $(grep $(hostname) /etc/hosts | cut -f1) d2e >> /etc/hosts && apt-get install -y -q pwgen dovecot-common dovecot-core dovecot-imapd opendkim opendkim-tools dovecot-sieve dovecot-lmtpd dovecot-managesieved dovecot-mysql spamassassin spamc dovecot-solr mysql-client libsasl2-modules dovecot-lucene iptables fail2ban

RUN mkdir /etc/postfix/
ADD postfix/master.cf /etc/postfix/master.cf
ADD postfix/main.cf /etc/postfix/main.cf
RUN  apt-get install -q -y postfix
RUN  apt-get install -y  -q postfix-pcre postfix-mysql postfix-policyd-spf-python

#SPAMASSASIN
RUN adduser --disabled-login --disabled-password  --gecos "" spamd
ADD spamassasin/spamassassin /etc/default/spamassassin
ADD spamassasin/local.cf /etc/spamassassin/local.cf

#SIEVE
RUN mkdir /var/lib/dovecot/sieve/
ADD dovecot/default.sieve /var/lib/dovecot/sieve/default.sieve
#RUN chown -R vmail:vmail /var/lib/dovecot
RUN mkdir /var/sieve-scripts
#RUN sievec /var/lib/dovecot/sieve/default.sieve

#Dovecot
ADD dovecot/dovecot.conf /etc/dovecot/dovecot.conf
ADD dovecot/dovecot-sql.conf.ext /etc/dovecot/
ADD opendkim/opendkim.conf /etc/opendkim.conf
ADD dovecot/dovecot /etc/init.d/dovecot
ADD dovecot/deldovecotuser /usr/local/sbin/
ADD dovecot/adddovecotuser /usr/local/sbin/
RUN chmod +x /etc/init.d/dovecot

#Postfix
ADD postfix/master.cf /etc/postfix/master.cf
ADD postfix/main.cf /etc/postfix/main.cf
RUN mkdir /etc/postfix/mysql
ADD postfix/mysql/virtual_alias_maps.cf /etc/postfix/mysql/virtual_alias_maps.cf
ADD postfix/mysql/virtual_domains_maps.cf /etc/postfix/mysql/virtual_domains_maps.cf
ADD postfix/mysql/virtual_mailbox_maps.cf /etc/postfix/mysql/virtual_mailbox_maps.cf
ADD postfix//postfix-wrapper.sh /usr/local/bin/
ADD postfix/header_checks /etc/postfix/header_checks
RUN mkdir /config
RUN touch /config/domain-s
RUN echo "$MAILNAME" >> /config/domain-s

RUN mkdir /etc/service/dovecot
COPY service/dovecot.sh /etc/service/dovecot/run
RUN chmod +x /etc/service/dovecot/run

RUN mkdir /etc/service/opendkim
COPY service/opendkim.sh /etc/service/opendkim/run
RUN chmod +x /etc/service/opendkim/run

RUN mkdir /etc/service/spamassasin
COPY service/spamassasin.sh /etc/service/spamassasin/run
RUN chmod +x /etc/service/spamassasin/run

RUN mkdir /etc/service/postfix
COPY service/postfix.sh /etc/service/postfix/run
RUN chmod +x /etc/service/postfix/run

#startup scripts
RUN mkdir -p /etc/my_init.d

COPY scripts/startup.sh /etc/my_init.d/startup.sh
RUN chmod +x /etc/my_init.d/startup.sh

COPY scripts/first_run.sh /etc/my_init.d/first_run.sh
RUN chmod +x /etc/my_init.d/first_run.sh

COPY scripts/logtime.sh /etc/my_init.d/logtime.sh
RUN chmod +x /etc/my_init.d/logtime.sh

COPY scripts/maintain.sh /tmp/maintain.sh
RUN chmod +x /tmp/maintain.sh
RUN /tmp/maintain.sh

#f2b
COPY fail2ban/jail.conf /etc/fail2ban/jail.conf
COPY fail2ban/filter.d/dovecot.conf /etc/fail2ban/filter.d/dovecot.conf
COPY fail2ban/filter.d/postfix.auth.conf /etc/fail2ban/filter.d/postfix.auth.conf
RUN echo "ignoreregex =" >> /etc/fail2ban/filter.d/postfix-sasl.conf && mkdir /var/run/fail2ban
RUN mkdir /etc/service/f2b
COPY service/f2b.sh /etc/service/f2b/run
COPY fail2ban/dovecot-pop3imap.conf /etc/fail2ban/filter.d/dovecot-pop3imap.conf
RUN chmod +x /etc/service/f2b/run
RUN touch /var/log/auth.log

#CRON
RUN mkdir /cron
COPY cron/solr_cron /cron
RUN  crontab /cron/solr_cron

EXPOSE 465 587 25 4190

VOLUME ["/home/vmail", "/var/log", "/etc/ssl", "/etc/postfix", "/etc/dovecot", "/etc/opendkim"]
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/sbin/my_init"]
