#!/bin/sh
# `/sbin/setuser memcache` runs the given command as the user `memcache`.
# If you omit that part, the command will be run as root.
exec spamd --create-prefs --max-children 2 --helper-home-dir --listen-ip=0.0.0.0 --allowed-ips=0.0.0.0/0 >>/var/log/spamassassin.log 2>&1