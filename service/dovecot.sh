#!/bin/sh
# `/sbin/setuser memcache` runs the given command as the user `memcache`.
# If you omit that part, the command will be run as root.
exec /usr/sbin/dovecot -c /etc/dovecot/dovecot.conf -F >>/var/log/dovecot.log 2>&1