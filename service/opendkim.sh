#!/bin/sh
# `/sbin/setuser memcache` runs the given command as the user `memcache`.
# If you omit that part, the command will be run as root.
exec opendkim -f -x /etc/opendkim.conf -p inet:12301@localhost >>/var/log/opendkim.log 2>&1
