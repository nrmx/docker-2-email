#!/usr/bin/env bash

# exit cleanly
trap "{ /usr/sbin/service postfix stop; }" EXIT
/usr/sbin/service postfix start
tail -f /var/log/mail.log

# don't exit
sleep infinity