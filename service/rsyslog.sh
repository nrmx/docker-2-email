#!/bin/sh
# `/sbin/setuser memcache` runs the given command as the user `memcache`.
# If you omit that part, the command will be run as root.
exec /usr/sbin/rsyslogd -n >>/var/log/rsyslog.log 2>&1