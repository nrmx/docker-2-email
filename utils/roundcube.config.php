<?php
$config = [];
$config['db_dsnw'] = 'sqlite:////var/db/roundcube.db?mode=0640';
$config['des_key'] = 'Change_me_I_am_example!!';
$config['plugins'] = [
    'archive',
    'zipdownload',
];

$config['default_host'] = 'ssl://ks.nyco.cc';

// required to ignore SSL cert. verification
// see: https://bbs.archlinux.org/viewtopic.php?id=187063
$config['imap_conn_options'] = array(
  'ssl' => array(
         'verify_peer'  => false,
         'verify_peer_name' => false,
   ),
);
$config['smtp_conn_options'] = array(
  'ssl' => array(
        'verify_peer'   => false,
        'verify_peer_name' => false,
  ),
);

$config['smtp_user'] = '%u';

// SMTP password (if required) if you use %p as the password Roundcube
// will use the current user's password for login
$config['smtp_pass'] = '%p';

// SMTP server just like IMAP server

$config['smtp_server'] = 'tls://%h';



