#!/bin/bash
sudo docker run --restart=always -e ROUNDCUBEMAIL_PLUGINS=managesieve -e ROUNDCUBEMAIL_DEFAULT_HOST=ks.nyco.cc -e ROUNDCUBEMAIL_DEFAULT_PORT=993 -e ROUNDCUBEMAIL_SMTP_SERVER=ks.nyco.cc --name rc -v /root/docker-2-email/rc/:/var/roundcube/config/ -e ROUNDCUBEMAIL_SMTP_PORT=587 -p 8090:80 -d roundcube/roundcubemail
