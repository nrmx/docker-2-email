version: "3"

services:
 mail:
   build: .
   #image: nmahadkar/email
   environment:
     - MAILNAME=ks.nyco.cc
     - MAILADDR=nrm@ks.nyco.cc
     - ROOTPW=safe1
     - MAINDB=vimbadmin
     - PASSWDDB=vimbadmin
     #- SMART_HOST=gmail.com.local
     #- SMART_HOST_PORT=587
     #- SMART_H_USER=me@gmail.com
     #- SMART_H_PASSWORD=my_gmail_password

   volumes:
     - /etc/postfix
     - /etc/dovecot
     - /etc/ssl
     - /etc/opendkim
     - /var/log/container:/var/log
     - vmail:/home/vmail/
     - ./config:/config/ 
     - dkim-keys:/etc/opendkim/keys/
   ports:
     - "25:25"
     - "587:587"
     - "465:465"
     - "993:993"
     - "4190:4190"
   networks:
     - network
   depends_on:
     - "mysql"
   restart: always
   container_name: email

 mysql:
   image: mysql:5.7
   environment:
    - MYSQL_ROOT_PASSWORD=safe1
   networks:
    - network
   restart: always
   container_name: mysql

 vimbadmin: 
   build: vimbadmin
   #image: nmahadkar/vimbadmin
   environment:
    - MYSQL_ROOT_PASSWORD=safe1
    - MYSQL_PORT_3306_TCP_ADDR=mysql
    - VIMBADMIN_SUPERUSER=nrm@ks.nyco.cc
    - VIMBADMIN_SUPERUSER_PASSWORD=axys67b+
    - MYSQL_DB_NAME=vimbadmin
    - MYSQL_DB_USERNAME=vimbadmin
    - MYSQL_DB_PASSWORD=5iveL!ves
    - VIMBADMIN_VERSION=3.0.12
    - APPLICATION_PATH=/var/www/html/vimbadmin
    - DOMAIN=ks.nyco.cc
   volumes:
    - vimbadmin:/srv
    - vmail:/home/vmail
   ports:
    - "8085:80" 
   networks:
    - network
 
 webmail: 
   build: rl
   #image: nmahadkar/rainloop
   ports:
    - "8086:80"
   networks:
    - network
   volumes:
    - ox-data:/ox-data o-x

 webmail-ox:
   build: ox
   #image: nmahadkar/rainloop
   ports:
    - "8087:80"
   networks:
    - network
   volumes:
    - ox-data:/ox-data o-x
   container_name: d2e-ox


 reverseproxy:
   build: proxy
   ports:
    - 80:80
    - 443:443
   restart: always
   networks:
    - network
 
networks:  
 network:
   driver: bridge

volumes:
  vmail:
  vimbadmin:
  ox-data:
  dkim-keys:
  ox-data:
